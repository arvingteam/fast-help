package ninja.meido.fasthelp.profile;

import ninja.meido.fasthelp.models.User;

/**
 * Created by ENRIQUE on 13/07/2016.
 */
public interface ProfileView {

    void showProfile(User user);
    void setPresenter(ProfilePresenter mPresenter);

}
