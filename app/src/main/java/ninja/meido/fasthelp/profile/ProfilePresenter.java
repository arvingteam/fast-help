package ninja.meido.fasthelp.profile;

public interface ProfilePresenter {
    void displayCurrentUser();
}
