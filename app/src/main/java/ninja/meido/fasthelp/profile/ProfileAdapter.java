package ninja.meido.fasthelp.profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ninja.meido.fasthelp.R;
import ninja.meido.fasthelp.models.User;

/**
 * Created by ENRIQUE on 15/07/2016.
 */
public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ViewHolder>{

    private final Context mContext;
    private final List<User> mValues;

    public ProfileAdapter (Context context, List<User> values){
        this.mContext=context;
        this.mValues = values;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_mi_perfil,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProfileAdapter.ViewHolder holder, int position) {

    }


    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public final View mView;
        public final ImageView iviPhoto;
        public final TextView tviProfileName;
        public final TextView tviPhoneNumber;
        public final TextView tviMail;


        public ViewHolder(View view) {
            super(view);
            mView=view;
            iviPhoto= (ImageView) view.findViewById(R.id.profilePhoto);
            tviProfileName = (TextView) view.findViewById(R.id.profileName);
            tviPhoneNumber = (TextView) view.findViewById(R.id.profilePhoneNumber);
            tviMail = (TextView) view.findViewById(R.id.profileMail);

        }
    }
}
