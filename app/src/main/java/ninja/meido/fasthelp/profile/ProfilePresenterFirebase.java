package ninja.meido.fasthelp.profile;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ninja.meido.fasthelp.models.User;

public class ProfilePresenterFirebase implements ProfilePresenter {
    private ProfileView mView;
    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;


    public ProfilePresenterFirebase(ProfileView view) {
        mView = view;
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
    }

    @Override
    public void displayCurrentUser() {
        FirebaseUser fUser = mAuth.getCurrentUser();
        DatabaseReference dbRef = mDatabase.getReference("users");

        dbRef.child(fUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                mView.showProfile(user);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("Profile","Failed to read value",databaseError.toException());
            }
        });
    }
}
