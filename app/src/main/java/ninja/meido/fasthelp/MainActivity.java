package ninja.meido.fasthelp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import ninja.meido.fasthelp.helperlist.HelperListFragment;
import ninja.meido.fasthelp.helperlist.pop.HelperListItemPopActivity;
import ninja.meido.fasthelp.helperlist.pop.HelperListItemPopFragment;
import ninja.meido.fasthelp.messaging.MensajesFragment;
import ninja.meido.fasthelp.profile.ProfileFragment;
import ninja.meido.fasthelp.mis_cursos.MisCursosFragment;
import ninja.meido.fasthelp.models.ContactDetail;
import ninja.meido.fasthelp.models.User;
import ninja.meido.fasthelp.modo_asesor.ModoAsesorFragment;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        HelperListFragment.OnUserListItemSelectedListener,
        HelperListItemPopFragment.OnUserItemInteractionListener {

    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.drawer_open, R.string.drawer_close);
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        navigationView.setNavigationItemSelectedListener(this);
        // Start main fragment and check in nav drawer
        onNavigationItemSelected(navigationView.getMenu().getItem(0));
        navigationView.getMenu().getItem(0).setChecked(true);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        item.setChecked(true);
        FragmentManager fManager = getSupportFragmentManager();
        FragmentTransaction fTransaction = fManager.beginTransaction();

        if (item.getItemId() == R.id.men_asesorias) {
            fTransaction.replace(R.id.content_frame, new HelperListFragment());
        } else if (item.getItemId() == R.id.men_mi_perfil) {
            fTransaction.replace(R.id.content_frame, new ProfileFragment());
        } else if (item.getItemId() == R.id.men_mis_cursos) {
            fTransaction.replace(R.id.content_frame, new MisCursosFragment());
        } else if (item.getItemId() == R.id.men_mensajes) {
            fTransaction.replace(R.id.content_frame, new MensajesFragment());
        } else if (item.getItemId() == R.id.men_modo_asesor) {
            fTransaction.replace(R.id.content_frame, new ModoAsesorFragment());
        }
        fTransaction.commit();
        mDrawerLayout.closeDrawers();
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.menSettings:
                Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onUserListClick(User user) {
        // Start new activity
        Intent intent = new Intent(this, HelperListItemPopActivity.class);
        intent.putExtra("user", user);
        startActivity(intent);
    }

    @Override
    public void onDetailInteraction(ContactDetail detail) {
        Intent intent;
        switch (detail.type) {
            case ContactDetail.TYPE_PHONE:
                intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + detail.content));
                startActivity(intent);
                break;
            case ContactDetail.TYPE_MAIL:
                intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:" + detail.content));
                startActivity(intent);
                break;
        }
    }
}

