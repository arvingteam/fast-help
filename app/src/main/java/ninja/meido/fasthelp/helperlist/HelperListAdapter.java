package ninja.meido.fasthelp.helperlist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ninja.meido.fasthelp.R;
import ninja.meido.fasthelp.models.User;

public class HelperListAdapter extends RecyclerView.Adapter<HelperListAdapter.ViewHolder> {

    private final Context mContext;
    private final List<User> mValues;
    private final ItemSelectedListener mListener;

    public HelperListAdapter(Context context,
                             ItemSelectedListener listener,
                             List<User> items) {
        mContext = context;
        mListener = listener;
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_helper_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        Picasso.with(mContext)
                .load(holder.mItem.getImageReferenceUrl())
                .fit()
                .centerCrop()
                .into(holder.iviUserPhoto);
        holder.tviDisplayName.setText(holder.mItem.getDisplayName());
        holder.tviCareer.setText(holder.mItem.getCareer());
        holder.rbRating.setRating(holder.mItem.getAverageScore());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onItemClick(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView iviUserPhoto;
        public final TextView tviDisplayName;
        public final TextView tviCareer;
        public final RatingBar rbRating;
        public User mItem;

        public ViewHolder(View view) {
            super(view);
            view.setClickable(true);

            mView = view;
            iviUserPhoto = (ImageView) view.findViewById(R.id.iviUserPhoto);
            tviDisplayName = (TextView) view.findViewById(R.id.tviDisplayName);
            tviCareer = (TextView) view.findViewById(R.id.tviCareer);
            rbRating = (RatingBar) view.findViewById(R.id.rbRating);
        }
    }

    public interface ItemSelectedListener {
        void onItemClick(User selectedUser);
    }
}
