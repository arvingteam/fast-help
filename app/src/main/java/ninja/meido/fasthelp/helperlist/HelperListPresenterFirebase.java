package ninja.meido.fasthelp.helperlist;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ninja.meido.fasthelp.models.ContactDetail;
import ninja.meido.fasthelp.models.User;

public class HelperListPresenterFirebase implements HelperListPresenter {
    private FirebaseDatabase database;
    private HelperListView mView;

    public HelperListPresenterFirebase(HelperListView mView) {
        this.mView = mView;
        database = FirebaseDatabase.getInstance();
    }

    @Override
    public void showList() {
        final List<User> userList = new ArrayList<>();

        DatabaseReference dbRef = database.getReference("users");
        dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    User user = userSnapshot.getValue(User.class);
                    if (user.getContactDetails() == null) user.setContactDetails(new ArrayList<ContactDetail>());
                    if (user.getContactDetails().contains(null)) user.getContactDetails().remove(null);
                    userList.add(user);
                }
                Collections.sort(userList, new Comparator<User>() {
                    @Override
                    public int compare(User user, User other) {
                        return Float.compare(other.getAverageScore(), user.getAverageScore());
                    }
                });
                mView.showList(userList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("HelperList", "Failed to read value.", databaseError.toException());
            }
        });
    }
}