package ninja.meido.fasthelp.helperlist;

import java.util.List;

import ninja.meido.fasthelp.models.User;

public interface HelperListView {
    void setPresenter(HelperListPresenter mPresenter);
    void showList(List<User> users);
}
