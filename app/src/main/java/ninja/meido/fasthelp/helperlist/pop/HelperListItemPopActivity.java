package ninja.meido.fasthelp.helperlist.pop;

import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ninja.meido.fasthelp.R;
import ninja.meido.fasthelp.models.ContactDetail;

public class HelperListItemPopActivity extends AppCompatActivity
        implements HelperListItemPopFragment.OnUserItemInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_helper_list_item_pop);
    }

    @Override
    public void onDetailInteraction(ContactDetail detail) {
        Intent intent;
        switch (detail.type) {
            case ContactDetail.TYPE_PHONE:
                intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + detail.content));
                startActivity(intent);
                break;
            case ContactDetail.TYPE_MAIL:
                intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:" + detail.content));
                startActivity(intent);
                break;
        }
    }

}
