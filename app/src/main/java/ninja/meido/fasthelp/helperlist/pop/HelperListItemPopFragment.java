package ninja.meido.fasthelp.helperlist.pop;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import ninja.meido.fasthelp.R;
import ninja.meido.fasthelp.models.ContactDetail;
import ninja.meido.fasthelp.models.User;

public class HelperListItemPopFragment extends Fragment
        implements HelperListItemPopAdapter.OnUserItemClickListener {

    private OnUserItemInteractionListener mListener;
    private RecyclerView mRecyclerView;
    private User user;
    private Context mContext;

    public HelperListItemPopFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_helper_list_item_pop, container, false);
        if (view.getTag().equals("main") || view.getTag().equals("xlarge")) { // Spawned within an activity for these cases
            user = (User) getActivity().getIntent().getSerializableExtra("user");
        } else {
            user = (User) getArguments().getSerializable("user");
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) getView().findViewById(R.id.item_contact_details_list);

        ImageView itemImage = (ImageView) getView().findViewById(R.id.item_image);
        Picasso.with(mContext)
                .load(user.getImageReferenceUrl())
                .fit()
                .centerCrop()
                .into(itemImage);

        // Check which view is running
        if (view.getTag().equals("xlarge-land")) {
            // xlarge-land variant
            TextView title = (TextView) view.findViewById(R.id.itemTitle);
            TextView subtitle1 = (TextView) view.findViewById(R.id.itemSubtitle1);
            TextView subtitle2 = (TextView) view.findViewById(R.id.itemSubtitle2);
            title.setText(user.getDisplayName());
            subtitle1.setText(user.getCareer());
            subtitle2.setText(user.getUniversity());
        } else if (view.getTag().equals("xlarge")) {
            // xlarge variant
            Toolbar toolbar = (Toolbar) getView().findViewById(R.id.item_toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().finish();
                }
            });
            TextView title = (TextView) view.findViewById(R.id.itemTitle);
            TextView subtitle1 = (TextView) view.findViewById(R.id.itemSubtitle1);
            TextView subtitle2 = (TextView) view.findViewById(R.id.itemSubtitle2);
            title.setText(user.getDisplayName());
            subtitle1.setText(user.getCareer());
            subtitle2.setText(user.getUniversity());
        } else {
            // main variant
            Toolbar toolbar = (Toolbar) getView().findViewById(R.id.item_toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().finish();
                }
            });
            toolbar.setTitle(user.getDisplayName());
        }
        mRecyclerView.setAdapter(
                new HelperListItemPopAdapter(getContext(), this, user.getContactDetails()));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        if (getActivity() instanceof OnUserItemInteractionListener) {
            mListener = (OnUserItemInteractionListener) getActivity();
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnUserItemInteractionListener");
        }
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
        mListener = null;
    }

    public interface OnUserItemInteractionListener {
        void onDetailInteraction(ContactDetail detail);
    }

    @Override
    public void onContactDetailClick(View view, ContactDetail contactDetail) {
        mListener.onDetailInteraction(contactDetail);
    }
}
