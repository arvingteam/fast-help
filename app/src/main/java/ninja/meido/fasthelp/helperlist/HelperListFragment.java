package ninja.meido.fasthelp.helperlist;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ninja.meido.fasthelp.R;
import ninja.meido.fasthelp.helperlist.pop.HelperListItemPopFragment;
import ninja.meido.fasthelp.helperlist.pop.HelperListItemPopFragmentEmpty;
import ninja.meido.fasthelp.models.ContactDetail;
import ninja.meido.fasthelp.models.User;

public class HelperListFragment extends Fragment implements HelperListView,
        HelperListAdapter.ItemSelectedListener,
        HelperListItemPopFragment.OnUserItemInteractionListener {

    private OnUserListItemSelectedListener mListener;
    private HelperListPresenter mPresenter;
    private RecyclerView mRecyclerView;

    public HelperListFragment() {
    }

    @Override
    public void setPresenter(HelperListPresenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void showList(List<User> users) {
         mRecyclerView.setAdapter(new HelperListAdapter(this.getContext(), this, users));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setPresenter(new HelperListPresenterFirebase(this));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_helper_list, container, false);
        Context context = view.getContext();

        if (view.getTag().equals("tablet-land")) {
            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            mRecyclerView = recyclerView;

            FragmentManager manager = getChildFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.listDetails, new HelperListItemPopFragmentEmpty());
            transaction.commit();
        } else if (view.getTag().equals("main")) {
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            mRecyclerView = recyclerView;
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter.showList();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnUserListItemSelectedListener) {
            mListener = (OnUserListItemSelectedListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnUserListItemSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(User selectedUser) {
        if (getView().getTag().equals("tablet-land")) {
            // Deal with it within the fragment
            FragmentManager manager = getChildFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();

            Bundle bundle = new Bundle();
            bundle.putSerializable("user", selectedUser);

            Fragment fragment = new HelperListItemPopFragment();
            fragment.setArguments(bundle);

            transaction.replace(R.id.listDetails, fragment);
            transaction.commit();
        } else {
            // Pass to activity
            mListener.onUserListClick(selectedUser);
        }

    }

    public interface OnUserListItemSelectedListener {
        void onUserListClick(User selectedUser);
    }

    @Override
    public void onDetailInteraction(ContactDetail detail) {

    }
}
