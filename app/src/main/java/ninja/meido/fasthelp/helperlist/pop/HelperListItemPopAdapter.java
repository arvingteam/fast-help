package ninja.meido.fasthelp.helperlist.pop;

import android.content.Context;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ninja.meido.fasthelp.R;
import ninja.meido.fasthelp.models.ContactDetail;

public class HelperListItemPopAdapter
        extends RecyclerView.Adapter<HelperListItemPopAdapter.ViewHolder> {

    private final OnUserItemClickListener mListener;
    private final Context mContext;
    private final List<ContactDetail> mValues;

    public HelperListItemPopAdapter(Context context, OnUserItemClickListener listener, List<ContactDetail> contactDetails) {
        this.mContext = context;
        this.mListener = listener;
        this.mValues = contactDetails;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_helper_list_item_pop_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        // Add an appropriate glyph
        switch (holder.mItem.type) {
            case ContactDetail.TYPE_PHONE:
                holder.iviDetailGlyph.setImageResource(R.drawable.ic_phone_black_24dp);
                break;
            case ContactDetail.TYPE_MAIL:
                holder.iviDetailGlyph.setImageResource(R.drawable.ic_email_black_24dp);
                break;
        }

        // Set text for detail
        holder.tviDetailPrimary.setText(holder.mItem.content);
        holder.tviDetailSecondary.setText(
                ContactsContract.CommonDataKinds.Phone.getTypeLabel(
                        mContext.getResources(), holder.mItem.contentType, "")
        );

        // Add listener
        holder.butDetailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onContactDetailClick(view, holder.mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView iviDetailGlyph;
        public final TextView tviDetailPrimary;
        public final TextView tviDetailSecondary;
        public final Button butDetailButton;
        public ContactDetail mItem;

        public ViewHolder(View view) {
            super(view);
            view.setClickable(true);

            this.mView = view;
            this.iviDetailGlyph = (ImageView) view.findViewById(R.id.detail_glyph);
            this.tviDetailPrimary = (TextView) view.findViewById(R.id.detail_primary);
            this.tviDetailSecondary = (TextView) view.findViewById(R.id.detail_secondary);
            this.butDetailButton = (Button) view.findViewById(R.id.butDetailButton);
        }
    }

    public interface OnUserItemClickListener {
        void onContactDetailClick(View view, ContactDetail contactDetail);
    }
}
