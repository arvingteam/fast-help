package ninja.meido.fasthelp.helperlist.pop;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ninja.meido.fasthelp.R;

public class HelperListItemPopFragmentEmpty extends Fragment {


    public HelperListItemPopFragmentEmpty() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_helper_list_item_pop_fragment_empty, container, false);
    }

}
