package ninja.meido.fasthelp.register.post;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Switch;

import ernestoyaquello.com.verticalstepperform.VerticalStepperFormLayout;
import ernestoyaquello.com.verticalstepperform.interfaces.VerticalStepperForm;
import ninja.meido.fasthelp.MainActivity;
import ninja.meido.fasthelp.R;
import ninja.meido.fasthelp.models.User;

public class PostRegisterActivity extends AppCompatActivity
        implements PostRegisterView, VerticalStepperForm {

    private User mUser;
    private PostRegisterPresenter mPresenter;
    private VerticalStepperFormLayout verticalStepperForm;

    private Switch mSwWantHelp;
    private Switch mSwWantToHelp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_register);

        Intent intent = getIntent();
        mUser = (User) intent.getSerializableExtra("user");

        // Set up stepper
        String[] steps = {"Asesoría"/*, "Email", "Phone Number"*/};
        int colorPrimary = ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary);
        int colorPrimaryDark = ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark);

        verticalStepperForm = (VerticalStepperFormLayout) findViewById(R.id.verticalStepperForm);

        VerticalStepperFormLayout.Builder.newInstance(verticalStepperForm, steps, this, this)
                .primaryColor(colorPrimary)
                .primaryDarkColor(colorPrimaryDark)
                .init();

        setPresenter(new PostRegisterPresenterFirebase(this));
    }

    @Override
    public void setPresenter(PostRegisterPresenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public View createStepContentView(int stepNumber) {
        View view = null;
        switch (stepNumber) {
            case 0:
                view = createSelectHelpModeView();
                break;
            case 1:
                view = createSelectHelpModeView(); //2
                break;
            case 2:
                view = createSelectHelpModeView(); //3
                break;
        }
        return view;
    }

    private View createSelectHelpModeView() {
        View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.stepper_help_step, null, false);
        mSwWantHelp = (Switch) view.findViewById(R.id.swWantHelp);
        mSwWantToHelp = (Switch) view.findViewById(R.id.swWantToHelp);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkHelpMode();
            }
        };
        mSwWantHelp.setOnClickListener(listener);
        mSwWantToHelp.setOnClickListener(listener);
        return view;
    }

    @Override
    public void onStepOpening(int stepNumber) {
        switch (stepNumber) {
            case 0:
                break;
            case 1:
                checkHelpMode();
                break;
            case 2:
                checkHelpMode();
                break;
        }
    }

    private void checkHelpMode() {
        mSwWantHelp = (Switch) findViewById(R.id.swWantHelp);
        mSwWantToHelp = (Switch) findViewById(R.id.swWantToHelp);

        Log.w("Checking", String.valueOf(mSwWantHelp.isChecked()));
        Log.w("Checking", String.valueOf(mSwWantToHelp.isChecked()));

        if (mSwWantHelp.isChecked() || mSwWantToHelp.isChecked()) {
            verticalStepperForm.setActiveStepAsCompleted();
        } else {
            verticalStepperForm.setActiveStepAsUncompleted("Seleccione al menos una opción.");
        }
    }

    @Override
    public void sendData() {
        mUser.setNeedsHelp(mSwWantHelp.isChecked());
        mUser.setOffersHelp(mSwWantToHelp.isChecked());

        mPresenter.saveUser(mUser);
    }

    @Override
    public void finishRegister() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
