package ninja.meido.fasthelp.register.post;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ninja.meido.fasthelp.models.User;

public class PostRegisterPresenterFirebase implements PostRegisterPresenter {
    private PostRegisterView mView;
    private FirebaseDatabase mDatabase;

    public PostRegisterPresenterFirebase(PostRegisterView view) {
        mView = view;
        mDatabase = FirebaseDatabase.getInstance();
    }

    @Override
    public void saveUser(User user) {
        Log.d("Register", user.toString());
        Log.d("Register", user.getUid());

        DatabaseReference dbRef = mDatabase.getReference("users");
        dbRef.child(user.getUid()).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mView.finishRegister();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof FirebaseNetworkException) {
                    Log.e("LoginPresenter", Log.getStackTraceString(e));
                    //mView.onNetworkError(R.string.network_error);
                } else {
                    Log.e("LoginPresenter", Log.getStackTraceString(e));
                }
            }
        });
    }
}
