package ninja.meido.fasthelp.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import ninja.meido.fasthelp.R;
import ninja.meido.fasthelp.models.User;
import ninja.meido.fasthelp.register.post.PostRegisterActivity;

public class RegisterActivity extends AppCompatActivity implements RegisterView {

    private RegisterPresenter mPresenter;
    private TextInputLayout registerDisplayNameLayout;
    private TextInputLayout registerMailLayout;
    private TextInputLayout registerPasswordLayout;
    private TextInputLayout registerConfirmPasswordLayout;
    private TextInputLayout registerUniversityLayout;
    private TextInputLayout registerCareerLayout;
    private EditText eteRegisterDisplayName;
    private EditText eteRegisterMail;
    private EditText eteRegisterPassword;
    private EditText eteRegisterConfirmPassword;
    private EditText eteRegisterUniversity;
    private EditText eteRegisterCareer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        registerDisplayNameLayout = (TextInputLayout) findViewById(R.id.registerDisplayNameLayout);
        registerMailLayout = (TextInputLayout) findViewById(R.id.registerMailLayout);
        registerPasswordLayout = (TextInputLayout) findViewById(R.id.registerPasswordLayout);
        registerConfirmPasswordLayout = (TextInputLayout) findViewById(R.id.registerConfirmPasswordLayout);
        registerUniversityLayout = (TextInputLayout) findViewById(R.id.registerUniversityLayout);
        registerCareerLayout = (TextInputLayout) findViewById(R.id.registerCareerLayout);

        eteRegisterDisplayName = (EditText) findViewById(R.id.eteRegisterDisplayName);
        eteRegisterMail = (EditText) findViewById(R.id.eteRegisterMail);
        eteRegisterPassword = (EditText) findViewById(R.id.eteRegisterPassword);
        eteRegisterConfirmPassword = (EditText) findViewById(R.id.eteRegisterConfirmPassword);
        eteRegisterUniversity = (EditText) findViewById(R.id.eteRegisterUniversity);
        eteRegisterCareer = (EditText) findViewById(R.id.eteRegisterCareer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.registerToolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        setPresenter(new RegisterPresenterFirebase(this));
    }

    @Override
    public void setPresenter(RegisterPresenter presenter) {
        mPresenter = presenter;
    }

    public void onRegisterButtonClick(View view) {
        eteRegisterDisplayName.addTextChangedListener(
                new TextFieldsWatcher(eteRegisterDisplayName, registerDisplayNameLayout));
        eteRegisterMail.addTextChangedListener(
                new TextFieldsWatcher(eteRegisterMail, registerMailLayout));
        eteRegisterPassword.addTextChangedListener(
                new TextFieldsWatcher(eteRegisterPassword, registerPasswordLayout));
        eteRegisterConfirmPassword.addTextChangedListener(
                new TextFieldsWatcher(eteRegisterConfirmPassword, registerConfirmPasswordLayout));
        eteRegisterUniversity.addTextChangedListener(
                new TextFieldsWatcher(eteRegisterUniversity, registerUniversityLayout));
        eteRegisterCareer.addTextChangedListener(
                new TextFieldsWatcher(eteRegisterCareer, registerCareerLayout));

        String displayName = eteRegisterDisplayName.getText().toString();
        String mail = eteRegisterMail.getText().toString();
        String password = eteRegisterPassword.getText().toString();
        String confirmPassword = eteRegisterConfirmPassword.getText().toString();
        String university = eteRegisterUniversity.getText().toString();
        String career = eteRegisterCareer.getText().toString();

        // This logic should be moved to the view layer for decoupling, but for now it'll stay here.
        boolean validFailed = false;
        if (displayName.isEmpty()) {
            validFailed = true;
            registerDisplayNameLayout.setError(getString(R.string.field_required));
        }
        if (mail.isEmpty()) {
            validFailed = true;
            registerMailLayout.setError(getString(R.string.field_required));
        }
        if (password.length() < 6) {
            validFailed = true;
            registerPasswordLayout.setError(getString(R.string.password_length_error));
        }
        if (password.isEmpty()) {
            validFailed = true;
            registerPasswordLayout.setError(getString(R.string.field_required));
        }
        if (!confirmPassword.equals(password)) {
            validFailed = true;
            registerConfirmPasswordLayout.setError(getString(R.string.confirm_password_error));
        }
        if (confirmPassword.isEmpty()) {
            validFailed = true;
            registerConfirmPasswordLayout.setError(getString(R.string.field_required));
        }
        if (university.isEmpty()) {
            validFailed = true;
            registerUniversityLayout.setError(getString(R.string.field_required));
        }
        if (career.isEmpty()) {
            validFailed = true;
            registerCareerLayout.setError(getString(R.string.field_required));
        }
        if (validFailed) return;
        mPresenter.registerUser(new User(displayName, mail, university, career), password);
    }

    @Override
    public void onSuccesfulRegister(User user) {
        Intent intent = new Intent(this, PostRegisterActivity.class);
        intent.putExtra("user", user);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onFailedRegister(int reasonString) {
        Toast.makeText(this, getString(reasonString), Toast.LENGTH_LONG).show();
    }

    public class TextFieldsWatcher implements TextWatcher {
        private EditText editText;
        private TextInputLayout layout;

        public TextFieldsWatcher(EditText editText, TextInputLayout layout) {
            this.editText = editText;
            this.layout = layout;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editText.getId() == eteRegisterPassword.getId()) {
                boolean v = validateEmpty() && validatePassword();
            } else if (editText.getId() == eteRegisterConfirmPassword.getId()) {
                boolean v = validateEmpty() && validateConfirmPassword();
            } else {
                validateEmpty();
            }
        }

        public boolean validateEmpty() {
            if (editText.getText().toString().isEmpty()) {
                layout.setError(getString(R.string.field_required));
                return false;
            } else {
                layout.setError(null);
                return true;
            }
        }

        public boolean validatePassword() {
            if (editText.getText().toString().length() < 6) {
                layout.setError(getString(R.string.password_length_error));
                return false;
            } else {
                layout.setError(null);
                return true;
            }
        }

        public boolean validateConfirmPassword() {
            if (!editText.getText().toString().equals(eteRegisterPassword.getText().toString())) {
                layout.setError(getString(R.string.confirm_password_error));
                return false;
            } else {
                layout.setError(null);
                return true;
            }
        }
    }
}
