package ninja.meido.fasthelp.register;

import ninja.meido.fasthelp.models.User;

public interface RegisterPresenter {
    void registerUser(User user, String password);
}
