package ninja.meido.fasthelp.register;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ninja.meido.fasthelp.R;
import ninja.meido.fasthelp.models.User;

public class RegisterPresenterFirebase implements RegisterPresenter {
    private RegisterView mView;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase mDatabase;

    public RegisterPresenterFirebase(RegisterView view) {
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mView = view;
    }

    @Override
    public void registerUser(final User user, final String password) {
        final DatabaseReference dbRef = mDatabase.getReference("users");

        mAuth.createUserWithEmailAndPassword(user.getEmail(), password)
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        dbRef.child(authResult.getUser().getUid()).setValue(user);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof FirebaseNetworkException) {
                            mView.onFailedRegister(R.string.network_error);
                        } else {
                            Log.e("LoginPresenter", Log.getStackTraceString(e));
                            mView.onFailedRegister(R.string.unknown_error);
                        }
                    }
                })
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        mAuth.signInWithEmailAndPassword(user.getEmail(), password)
                                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        user.setUid(mAuth.getCurrentUser().getUid());
                                        mView.onSuccesfulRegister(user);
                                    }
                                });
                    }
                });
    }
}
