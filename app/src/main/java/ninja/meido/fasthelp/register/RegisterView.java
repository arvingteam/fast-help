package ninja.meido.fasthelp.register;

import ninja.meido.fasthelp.models.User;

public interface RegisterView {
    void setPresenter(RegisterPresenter presenter);
    void onSuccesfulRegister(User user);
    void onFailedRegister(int reasonString);
}
