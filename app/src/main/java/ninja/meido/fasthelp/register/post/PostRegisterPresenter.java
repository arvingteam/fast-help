package ninja.meido.fasthelp.register.post;

import ninja.meido.fasthelp.models.User;

public interface PostRegisterPresenter {
    void saveUser(User user);
}
