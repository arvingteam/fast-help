package ninja.meido.fasthelp.register.post;

public interface PostRegisterView {
    void setPresenter(PostRegisterPresenter presenter);
    void finishRegister();
}
