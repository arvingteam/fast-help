package ninja.meido.fasthelp.login;

public interface LoginView {
    void setPresenter(LoginPresenter presenter);
    void onSuccesfulLogin();
    void onFailedLogin(int reasonString);
}
