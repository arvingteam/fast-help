package ninja.meido.fasthelp.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import ninja.meido.fasthelp.MainActivity;
import ninja.meido.fasthelp.R;
import ninja.meido.fasthelp.register.RegisterActivity;

public class LoginActivity extends AppCompatActivity implements LoginView {

    private LoginPresenter mPresenter;
    private EditText mEteLoginUser;
    private EditText mEteLoginPassword;
    private ProgressDialog mDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setPresenter(new LoginPresenterFirebase(this));
        checkPlayServices();

        mEteLoginUser = (EditText) findViewById(R.id.eteLoginUser);
        mEteLoginPassword = (EditText) findViewById(R.id.eteLoginPassword);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
    }

    @Override
    public void onSuccesfulLogin() {
        mDialog.dismiss();
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onFailedLogin(int reasonString) {
        Toast.makeText(this, getString(reasonString), Toast.LENGTH_LONG).show();
    }

    public void onLoginClick(View view) {
        if (mEteLoginUser.getText().toString().isEmpty() || mEteLoginPassword.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.login_empty_error), Toast.LENGTH_SHORT).show();
            return;
        }
        mDialog = ProgressDialog.show(this, "Autenticando", "Autenticando, por favor espere", true, false);
        mPresenter.userLogin(mEteLoginUser.getText().toString(), mEteLoginPassword.getText().toString());
    }

    public void onRegisterClick(View view){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    @Override
    public void setPresenter(LoginPresenter presenter) {
        mPresenter = presenter;
    }

    public void checkPlayServices() {
        int playServices = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if (playServices != ConnectionResult.SUCCESS) {
            GoogleApiAvailability.getInstance().showErrorNotification(this, playServices);
        }
    }
}
