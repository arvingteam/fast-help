package ninja.meido.fasthelp.login;

public interface LoginPresenter {
    void userLogin(String username, String password);
}
