package ninja.meido.fasthelp.login;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;

import ninja.meido.fasthelp.R;

public class LoginPresenterFirebase implements LoginPresenter {
    private LoginView mView;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    public LoginPresenterFirebase(LoginView view) {
        this.mView = view;
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void userLogin(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        mView.onSuccesfulLogin();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof FirebaseAuthException) {
                            mView.onFailedLogin(R.string.login_auth_error);
                        } else if (e instanceof FirebaseNetworkException) {
                            mView.onFailedLogin(R.string.network_error);
                        } else {
                            Log.e("LoginPresenter", Log.getStackTraceString(e));
                            mView.onFailedLogin(R.string.unknown_error);
                        }
                    }
                });
    }
}
