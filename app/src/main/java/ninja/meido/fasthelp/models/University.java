package ninja.meido.fasthelp.models;

import java.util.List;

public class University {
    public String unid;
    public String name;
    public List< Career > careers;

    public University() {
    }

    public University(String unid, String name, List<Career> careers) {
        this.unid = unid;
        this.name = name;
        this.careers = careers;
    }

    public class Career {
        public String cid;
        public String name;

        public Career() {
        }

        public Career(String cid, String name) {
            this.cid = cid;
            this.name = name;
        }
    }
}
