package ninja.meido.fasthelp.models;

import java.io.Serializable;
import java.util.List;

public class User implements Serializable {
    private String uid;
    private String email; // this is not a public contact detail, this is the address used to register the user
    private String displayName;
    private String imageReferenceUrl;

    private String university;
    private String career;

    private Boolean needsHelp;
    private Boolean offersHelp;

    private List< ContactDetail > contactDetails;

    private List< String > subjectList;
    private List< String > helperSubjectList;

    private List< Rating > ratingList;
    private float averageScore;

    public User() {
    }

    public User(String displayName, String email, String university, String career) {
        this.displayName = displayName;
        this.email = email;
        this.university = university;
        this.career = career;
    }

    public User(String uid, String email, String displayName, String imageReferenceUrl, String university, String career, Boolean needsHelp, Boolean offersHelp, List<ContactDetail> contactDetails, List<String> subjectList, List<String> helperSubjectList, List<Rating> ratingList, float averageScore) {
        this.uid = uid;
        this.email = email;
        this.displayName = displayName;
        this.imageReferenceUrl = imageReferenceUrl;
        this.university = university;
        this.career = career;
        this.needsHelp = needsHelp;
        this.offersHelp = offersHelp;
        this.contactDetails = contactDetails;
        this.subjectList = subjectList;
        this.helperSubjectList = helperSubjectList;
        this.ratingList = ratingList;
        this.averageScore = averageScore;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getImageReferenceUrl() {
        return imageReferenceUrl;
    }

    public void setImageReferenceUrl(String imageReferenceUrl) {
        this.imageReferenceUrl = imageReferenceUrl;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    public Boolean getNeedsHelp() {
        return needsHelp;
    }

    public void setNeedsHelp(Boolean needsHelp) {
        this.needsHelp = needsHelp;
    }

    public Boolean getOffersHelp() {
        return offersHelp;
    }

    public void setOffersHelp(Boolean offersHelp) {
        this.offersHelp = offersHelp;
    }

    public List<ContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public List<String> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(List<String> subjectList) {
        this.subjectList = subjectList;
    }

    public List<String> getHelperSubjectList() {
        return helperSubjectList;
    }

    public void setHelperSubjectList(List<String> helperSubjectList) {
        this.helperSubjectList = helperSubjectList;
    }

    public List<Rating> getRatingList() {
        return ratingList;
    }

    public void setRatingList(List<Rating> ratingList) {
        this.ratingList = ratingList;
    }

    public float getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(float averageScore) {
        this.averageScore = averageScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return uid.equals(user.uid);
    }

    @Override
    public int hashCode() {
        return uid.hashCode();
    }
}
