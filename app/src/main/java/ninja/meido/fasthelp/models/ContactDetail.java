package ninja.meido.fasthelp.models;

import java.io.Serializable;

public class ContactDetail implements Serializable {
    public String content;
    // ContactsContract.CommonDataKinds.Phone.TYPE_*
    // ContactsContract.CommonDataKinds.Email.TYPE_*
    public Integer contentType;
    public int type;

    public static final int TYPE_PHONE = 1;
    public static final int TYPE_MAIL = 2;

    public ContactDetail() {
    }

    public ContactDetail(String content, Integer contentType, int type) {
        this.content = content;
        this.contentType = contentType;
        this.type = type;
    }
}
