package ninja.meido.fasthelp.models;

import java.io.Serializable;

public class Rating implements Serializable {
    private Integer posterUid;
    private Integer reviewedUid;
    private Integer rating;
    private String ratingText;

    public Rating() {
    }

    public Rating(Integer posterUid, Integer reviewedUid, Integer rating, String ratingText) {
        this.posterUid = posterUid;
        this.reviewedUid = reviewedUid;
        this.rating = rating;
        this.ratingText = ratingText;
    }

    public Integer getPosterUid() {
        return posterUid;
    }

    public void setPosterUid(Integer posterUid) {
        this.posterUid = posterUid;
    }

    public Integer getReviewedUid() {
        return reviewedUid;
    }

    public void setReviewedUid(Integer reviewedUid) {
        this.reviewedUid = reviewedUid;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getRatingText() {
        return ratingText;
    }

    public void setRatingText(String ratingText) {
        this.ratingText = ratingText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rating rating = (Rating) o;
        return posterUid.equals(rating.posterUid) && reviewedUid.equals(rating.reviewedUid);
    }

    @Override
    public int hashCode() {
        int result = posterUid.hashCode();
        result = 31 * result + reviewedUid.hashCode();
        return result;
    }
}