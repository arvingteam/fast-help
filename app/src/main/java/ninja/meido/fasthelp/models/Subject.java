package ninja.meido.fasthelp.models;

public class Subject {
    public String sid;
    public String name;
    public String career;
    public String university;
    public float viewScore;

    public Subject() {
    }

    public Subject(String sid, String name, String career, String university, float viewScore) {
        this.sid = sid;
        this.name = name;
        this.career = career;
        this.university = university;
        this.viewScore = viewScore;
    }
}
